package com.truckla.cars.model;

import javax.persistence.*;

@Entity
@Table(name = "CARS")
@SequenceGenerator(name="seq", initialValue=4, allocationSize=100)
public class Car {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private Long id;
    String manufacturer;
    String model;
    String build;

    public Car() {
    }

    public Car(Long id, String manufacturer, String model, String build) {
        this.id = id;
        this.manufacturer = manufacturer;
        this.model = model;
        this.build = build;
    }

    public Long getId() {
        return id;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public String getBuild() {
        return build;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setBuild(String build) {
        this.build = build;
    }
}
